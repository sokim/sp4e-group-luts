#ifndef FFT_HH
#define FFT_HH
/* ------------------------------------------------------ */
#include "matrix.hh"
#include "my_types.hh"
#include <fftw3.h>
/* ------------------------------------------------------ */

struct FFT {

  static Matrix<complex> transform(Matrix<complex>& m);
  static Matrix<complex> itransform(Matrix<complex>& m);

  static Matrix<std::complex<int>> computeFrequencies(int size);
};

/* ------------------------------------------------------ */

inline Matrix<complex> FFT::transform(Matrix<complex>& m_in) {
    //input matrix size
    int N_ROW = m_in.rows();
    int N_COL = m_in.cols();
    //output matrix with size same with input matrix
    Matrix<complex> m_out(m_in.size());

    // 2D DFT using FFTW
    fftw_plan p;
    p = fftw_plan_dft_2d(N_ROW, N_COL,
                         reinterpret_cast<fftw_complex*>(m_in.data()),
                         reinterpret_cast<fftw_complex*>(m_out.data()),
                         FFTW_FORWARD, FFTW_ESTIMATE);

    fftw_execute(p);
    fftw_destroy_plan(p);
    // return non-normalized result
    return m_out;
}

/* ------------------------------------------------------ */

inline Matrix<complex> FFT::itransform(Matrix<complex>& m_in) {
    //input matrix size
    int N_ROW = m_in.rows();
    int N_COL = m_in.cols();
    //output matrix with size same with input matrix
    Matrix<complex> m_out(m_in.size());

    // 2D DFT using FFTW
    fftw_plan p;
    p = fftw_plan_dft_2d(N_ROW, N_COL,
                         reinterpret_cast<fftw_complex *>(m_in.data()),
                         reinterpret_cast<fftw_complex *>(m_out.data()),
                         FFTW_BACKWARD, FFTW_ESTIMATE);
    fftw_execute(p);

    fftw_destroy_plan(p);
    // return normalized result
    m_out /= N_ROW * N_COL;

    return m_out;
}

/* ------------------------------------------------------ */

inline Matrix<std::complex<int>> FFT::computeFrequencies(int size) {
    Matrix<std::complex<int>> m(size);
    int k, l;

    for (auto &&entry : index(m)) {
        int i = std::get<0>(entry);
        int j = std::get<1>(entry);
        auto &val = std::get<2>(entry);
        if(i <= size / 2){
            k = i;
            if (j <= size/2) l =j;
            else l= j-size;
        }
        else{
            k = i - size;
            if (j <= size/2) l =j;
            else l= j-size;
        }
        val = std::complex<int>(k, l);
    }
    return m;
}

#endif  // FFT_HH
