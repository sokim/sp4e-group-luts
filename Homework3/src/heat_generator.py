#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec  7 19:49:34 2020

@author: dimitris
"""

'''
    Script to create a csv file ("x" "y" "h_v") for radial heat distribution given R and domain   
'''
import sys
import argparse
import csv 
import numpy as np
import matplotlib.pyplot as plt

def main():

    parser = argparse.ArgumentParser(description='Generate csv file for radial unit heat distribution given R and domain')
    parser.add_argument('-R', nargs=1, default=[1], type=float, help=('Radius of the radial heat distribution [default R=1]')) #Non_negative
    parser.add_argument('-x', nargs=2, default=(-1,1), metavar=('x_min','x_max'), type=float, help=('Domain range in x-coordinates. Type <x_min, x_max> [default (-1,1)]')) 
    parser.add_argument('-y', nargs=2, default=(-1,1), metavar=('y_min','y_max'),type=float, help=('Domain range in y-coordinates. Type <y_min, y_max> [default (-1,1)]')) 
    parser.add_argument('-Nx', nargs=1, default=[30], type=int, help=('Number of points in the range of x. Grid will be [Nx x Ny]'))
    parser.add_argument('-Ny', nargs=1, default=[30], type=int, help=('Number of points in the range of y.Grid will be [Nx x Ny]')) 
    parser.add_argument('--show_plot', action='store_true', default=False, help='Show the plot of the iteration by setting this option.')
    parser.add_argument('-f', '--filename', type=str, required=False, default="heat.csv",
                        help=('Name of file to generate (please include csv extension: e.g. [-f myfile.csv]). Default: heat.csv)'))
    
    args = parser.parse_args()
    filename = args.filename
    R = args.R
    Nx = args.Nx
    Ny = args.Ny
    show_plot = args.show_plot
    XLims = args.x
    YLims = args.y


    #Check argument validity
    if R[0]<0 : sys.exit("[Argument Error]: Radius R cannot be negative.")
    if XLims[0]>XLims[1] : sys.exit("[Argument Error]: x_min > x_max! x_min should be <= x_max.")
    if YLims[0]>YLims[1] : sys.exit("[Argument Error]: y_min > y_max! y_min should be <= y_max.")
    if Nx[0]< 2: sys.exit("[Argument Error]: Number of points in x range must be >=2 (min range [2x2])") 
    if Ny[0]< 2: sys.exit("[Argument Error]: Number of points in y range must be >=2 (min range [2x2])") 

    #Generate point coordinates from range
    X = np.linspace(XLims[0],XLims[1],num=Nx[0])
    Y = np.linspace(YLims[0],YLims[1],num=Ny[0])
    hv = []
    x_coor = []
    y_coor = []
    theta = []
    isBound = []
    # create output file 
    with open(filename, mode='w', newline='') as csv_file:
        file_writer = csv.writer(csv_file, delimiter = ',', quotechar='"',quoting=csv.QUOTE_MINIMAL)
        for i in range(0,len(X)):
            for j in range(0,len(Y)):
                x_coor.append(X[i])
                y_coor.append(Y[j])
                hv.append(heat_function(X[i],Y[j],R[0]))
                theta.append(init_temperature(X[i],Y[j]))
                isBound.append(isBoundary(X[i], Y[j], XLims[0], XLims[1], YLims[0], YLims[1]))
                file_writer.writerow([X[i], Y[j], theta[-1], hv[-1], isBound[-1]])
                                    
    #Create graph to visually check result
    if show_plot:
        fig = plt.figure()
        axe = fig.add_subplot(1,1,1)
        for i in range(0,len(hv)):
            if hv[i]==0: 
                axe.scatter(x_coor[i],y_coor[i],color=[0,0,1])
            else:
                axe.scatter(x_coor[i],y_coor[i],color=[0,1,0])

        axe.set_aspect(1)
        plt.show()
    
    print('Execution Completed.')
    
    return 0
    
def heat_function(x,y,R):
    '''
        Function for the heat source rate of every particle 
        
        Parameters
        ----------
        x: x-coordinate (Real)
        y: y-coordinate (Real)
        R: Radius (Real)
        
        Returns 
        --------
        h_v: the heat source of the point (x,y)
    '''
    #if x**2 + y**2 < R**2 : #(for circular pattern with Radius R)
    if (x**2 + y**2 < R):
        h_v = 1
    else:
        h_v = 0    
    return h_v 

def init_temperature(x,y):
    '''
        Function for the intial temperature of every particle 
        
        Parameters
        ----------
        x: x-coordinate (Real)
        y: y-coordinate (Real)
        
        Returns 
        --------
        theta: the initial temperature of the point (x,y)
    '''
    #Implement a function of theta = f(x,y): 
    theta = 1 
    return theta 

def isBoundary(x,y,x_min,x_max,y_min,y_max):
    '''
        Function for the intial temperature of every particle 
        
        Parameters
        ----------
        x           : x-coordinate (Real)
        y           : y-coordinate (Real)
        x_min, x_max: the boundaries in x (Real)
        y_min, y_max: the boundaries in y (Real)
        
        Returns 
        --------
        isBound: (bool) 1: if point is in boundaries of the domain, 0 else 
    '''
    
    if ((x==x_min) or (x==x_max) or (y==y_min) or (y==y_max)) :
        isBound = 1
    else:
        isBound = 0 
        
    return isBound
            

if __name__ == "__main__":
    main()