//
// Created by Sohyeong Kim on 08.12.20.
//

#include "compute_temperature.hh"
#include "fft.hh"
#include "material_point.hh"
#include <cmath>
#include <vector>

/* -------------------------------------------------------------------------- */

void ComputeTemperature::setDeltaT(Real dt) { this->dt = dt; }

/* -------------------------------------------------------------------------- */

void ComputeTemperature::setHeatCapacity(Real C){this->C = C; }

/* -------------------------------------------------------------------------- */

void ComputeTemperature::setMassDensity(Real rho){this->rho = rho;}

/* -------------------------------------------------------------------------- */

void ComputeTemperature::setHeatConductivity(Real kappa){this->kappa = kappa;}

/* -------------------------------------------------------------------------- */
void ComputeTemperature::setDomainSize(Real L) {this->L = L;}

/* -------------------------------------------------------------------------- */

void ComputeTemperature::compute(System& system) {
    UInt n_particles = system.getNbParticles();
    UInt N = std::sqrt(n_particles);

    // --- Perform one full step of time integration ---
    /*******************************************************/
    //[0]. Create Matrix for computations
    Matrix<complex> m_in(N); // Temperature in real space
    Matrix<complex> m_out(N); // Temperature after FFT
    Matrix<complex> hv_in(N); // Heat rate in real space
    Matrix<complex> hv_out(N); // Heat rate after FFT

    Matrix<complex> pDevTheta; // intermediate term
    Matrix<complex> pDevThetaHat(N); // intermediate term

    /*******************************************************/
    //[1]. Initialize the temperature and heat rate
    for (auto&& entry : index(m_in)) {
        int i = std::get<0>(entry);
        int j = std::get<1>(entry);
        auto& p = static_cast<MaterialPoint&>( system.getParticle(i * N + j) );
        m_in(i, j) = p.getTemperature();
        hv_in(i, j) = p.getHeatRate();
    }

    //[2]. Call DFT for temperatures Matrix + heat source
    m_out = FFT::transform(m_in);
    hv_out = FFT::transform(hv_in);

    /*******************************************************/
    //[3]. Calculate pDev_tempHat_over_time from solving equation (for every point)
    // Calculate frequencies for qx and qy
    Matrix<std::complex<int>> freq = FFT::computeFrequencies(N);
    // Loop over particles and compute derivative
    for (UInt j = 0; j<N; j++){
        for (UInt i = 0; i<N; i++){
            double coeff = 2.*M_PI/L;
            double q = pow(coeff * freq(i, j).real(), 2) + pow(coeff * freq(i, j).imag(), 2);
            pDevThetaHat(i, j) = 1.0/(rho*C)*(hv_out(i, j) - kappa*m_out(i, j)*q);
        }
    }
    //[4]. Do inverse DFT
    pDevTheta = FFT::itransform(pDevThetaHat);

    /*******************************************************/
    //[5]. Update temperatures of all particles (add values to storage)
    for (auto&& entry : index(pDevTheta)) {
        int i = std::get<0>(entry);
        int j = std::get<1>(entry);

        auto& p = static_cast<MaterialPoint&>( system.getParticle(i * N + j) );
        if(p.isBoundary()) {//If the particles is position at the boundaries, set the temperature to zero
            p.getTemperature() = 0.;
        }
        else{ //Update temperatures to particles
            p.getTemperature()+= dt * pDevTheta(i,j).real();
        }
    }

}

/* -------------------------------------------------------------------------- */