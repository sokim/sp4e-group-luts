#ifndef __COMPUTE_TEMPERATURE__HH__
#define __COMPUTE_TEMPERATURE__HH__

/* -------------------------------------------------------------------------- */
#include "compute.hh"

//! Compute contact interaction between ping-pong balls
class ComputeTemperature : public Compute {

    // Virtual implementation
public:
    //! Penalty contact implementation
    void compute(System& system) override;
    //! Set time step
    void setDeltaT(Real dt);
    //! Set heat Capacity
    void setHeatCapacity(Real C);
    //! Set Mass Density
    void setMassDensity(Real rho);
    //! Set Heat Conductivity
    void setHeatConductivity(Real kappa);
    //! Set Heat Conductivity
    void setDomainSize(Real L);

private:
    //! Time step
    Real dt;
    //! Heat Capacity
    Real C;
    //! Heat Conductivity
    Real kappa;
    //! Mass Density
    Real rho;
    //! Domain Size
    Real L;
};

/* -------------------------------------------------------------------------- */
#endif  //__COMPUTE_TEMPERATURE__HH__
