////
//// Created by Sohyeong Kim on 07.12.20.
////

#include "compute_temperature.hh"
#include "my_types.hh"
#include "csv_reader.hh"
#include "csv_writer.hh"
#include "material_point.hh"
#include "material_points_factory.hh"
#include "system.hh"
#include <gtest/gtest.h>
#include <cmath>

/*****************************************************************/
class Solver : public ::testing::Test {
protected:
    void SetUp() override {
        // Setup initialization
        MaterialPointsFactory::getInstance();
        n_points = 256;
        L = 2.;

        //Initialize the points
        for (UInt j = 0; j < n_points; j++) {
            for (UInt i = 0; i < n_points; i++) {
                MaterialPoint p;
                p.getPosition()[0] = (double(i+0.5)/n_points - 0.5) * L;
                p.getPosition()[1] = (double(j+0.5)/n_points - 0.5) * L;

                 // update the boundary conditions
                if (p.getPosition()[0] == -L/2. || p.getPosition()[0] == L/2.
                || p.getPosition()[1] == -L/2. || p.getPosition()[1] == L/2.){
                    p.isBoundary() = 1;
                }else{
                    p.isBoundary() = 0;
                }

                points.push_back(p);
            }
        }

        // Set up the temperature computation
        temperature = std::make_shared<ComputeTemperature>();
        temperature->setDeltaT(1e-6);
        temperature->setMassDensity(1.0);
        temperature->setHeatCapacity(1.0);
        temperature->setHeatConductivity(1.0);
        temperature->setDomainSize(L);

    }

    System system;
    UInt n_points;
    Real L;
    std::vector<MaterialPoint> points;
    std::shared_ptr<ComputeTemperature> temperature;
};
/*****************************************************************/
TEST_F(Solver, homogeneous) {
    Real initial_temperature=20.;
    UInt time_steps=10;

    // Add particle to system
    for (auto &p : points) {
        p.getTemperature() = initial_temperature;
        p.getHeatRate() = 0.;
        system.addParticle(std::make_shared<MaterialPoint>(p));
    }

    // Uniform temperature without heat flux
    for (UInt i = 0; i < time_steps; ++i) {
        temperature->compute(system);
    }

    // Temperature should not change after few time steps
    for (auto &p : system){
        auto &mpoint = static_cast<MaterialPoint&>(p);
        ASSERT_NEAR(mpoint.getTemperature(), initial_temperature, 1e-15);
    }

    std::cout << "(Ex4-2) Passing the homogeneous temperature state " << std::endl;
}
/*****************************************************************/
TEST_F(Solver, sinusoidal) {
    UInt time_steps=10;

    // Add particle to system
    for (auto &p : points) {
        double x = p.getPosition()[0];
        p.getTemperature()=sin(2*M_PI*x/L);
        p.getHeatRate()=pow((2*M_PI)/L,2)*sin(2*M_PI*(x/L));

        system.addParticle(std::make_shared<MaterialPoint>(p));
    }

    // with heat flux
    for (UInt i = 0; i < time_steps; ++i) {
        temperature->compute(system);
    }

    // Temperature should not change after few time steps
    for (auto &p : system){
        auto &mpoint = static_cast<MaterialPoint&>(p);
        double x = mpoint.getPosition()[0];
        ASSERT_NEAR(mpoint.getTemperature(), sin(2*M_PI*x/L), 1e-5);
    }

    std::cout << "(Ex4-3) Passing the sinusoidal temperature state " << std::endl;
}
/*****************************************************************/
TEST_F(Solver, boundary) {
    UInt time_steps=10;

    // Add particle to system
    for (auto &p : points) {
        double x = p.getPosition()[0];
        if(x<=-0.5){
            p.getTemperature()=-x-1.;
        }else if(-0.5<x && x<=0.5){
            p.getTemperature()=x;
        }else{
            p.getTemperature()=-x+1.;
        }

        if(x==0.5){
            p.getHeatRate()=1.;
        }else if(x==-0.5){
            p.getHeatRate()=-1.;
        }else{
            p.getHeatRate()=0.;
        }

        system.addParticle(std::make_shared<MaterialPoint>(p));
    }

    // with heat flux
    for (UInt i = 0; i < time_steps; ++i) {
        temperature->compute(system);
    }

    // Temperature should not change after few time steps
    Real equilibrium_temperature;
    for (auto &p : system){
        auto &mpoint = static_cast<MaterialPoint&>(p);
        double x = mpoint.getPosition()[0];
        if(x<=-0.5){
            equilibrium_temperature=-x-1.;
        }else if(-0.5<x && x<=0.5){
            equilibrium_temperature=x;
        }else{
            equilibrium_temperature=-x+1.;
        }
        ASSERT_NEAR(mpoint.getTemperature(), equilibrium_temperature, 1e-2);
    }

    std::cout << "(Ex4-4) Passing the boundary temperature state " << std::endl;
}