# Homework 3. External libraries: application to a heat equation solver

### Students 
Sohyeong Kim, Dimitrios Tsitsokas
 
## How to build and execute C++ files
### Building
``` commandline
$ mkdir build && cd build
$ cmake .. -DCMAKE_BUILD_TYPE=Release
$ make
```
When compiled successfully, the execution files will be created in `Homework3/build/src/`.

### Testing for FFTW implementation (exercise 3):
The execution file `test_fft` will be created in `Homework3/build/src/` after the successful compile.
When executed, it will perform googletest on the FFT implementations such as forward, backward, and frequencies computations.  
``` commandline
$ ./test_fft
```
Note that FFT frequencies are compared with the expected output of numpy.fft.fftfreq 
as indicated in the [document](https://numpy.org/doc/stable/reference/generated/numpy.fft.fftfreq.html).
 
### Testing for temperature computation (exercise 4):
The execution file `test_heat` will be created in `Homework3/build/src/` after the successful compile.
When executed, it will perform googletest on the temperature computation in various heat source situations 
as described in exercise 4-2, 4-3, and 4-4.
  
``` commandline
$ ./test_heat
```

## How to execute python file (exercise 4-5)
Python file `heat_generator.py` generates a csv file of the format < x, y, theta(x,y) h_v(x,y), isBoundary(x,y) >, where x any y are the coordinates of the particle, theta(x,y) is the initial temperature, h_v(x,y) is the respective source heat rate given by Eq. (5) of the exercise description abd isBoundary(x,y) is a boolean indicator of whether the particle belongs to the boundary of the domain. In the current implementation,  theta(x,y) is implemented as homogeneous unit temperature (it can be customized in the init_temperature function defined in the script). 

To visually verify the results, a scatter plot can be generated (optionally) where the considered grid points are displayed in different colors (in blue if h_v = 0 and in green if h_v = 1). 

### Dependencies
- Numpy 
- csv

### Execution of python code heat_generator.py 
``` commandline
$ ./heat_generator.py --R [Radius] --x [(x_min, x_max)] --y [(y_min, y_max)] -Nx [No_of_Points] -Ny [No_of_Points] -f [filename] --show_plot 
```
##### Arguments list 
* `--R` is the radius of the radial heat distribution [default 1]
* `--x` is the domain range in x-coordinates. [default (-1,1)]
* `--y` is the domain range in y-coordinates. [default (-1,1)]
* `--Nx` is the number of points in the range of x [default 30].
* `--Ny` is the number of points in the range of y [default 30].
* `--f` is the name of file to generate. Make sure to add extension of the file.[default heat.csv]
* `--show_plot` is a flag to generate a 2-d scatter plot to visualize points with heat rate equal to zero (in blue) and 1 (in green) [optional]. Not recommended for N>50 as the current implementation of the plot is time consumming. 

### Examples of execution
``` commandline
$ ./heat_generator 
```
No arguments given (only default values used). Generates a csv file named "heat.csv" for the unit heat with R = 1, in the x-y domain [-1,1]x[-1,1] for Nx=Ny=30 (grid 30x30 -> 900 points). 

``` commandline
$ ./heat_generator.py -R 0.75 -x -1 2 -y -4 2 -Nx 30 -Ny 60 
```
Generates a csv file named "heat.csv" for the unit heat rate with R = 0.75, in the x-y domain [-1,2]x[-4,2] for Nx=30, Ny=60 (grid 30x60 -> 1800 points). 

``` commandline
$ ./heat_generator.py -R 2.25 -x -2 2 -y -2 2 -f myfile.csv --show_plot
```
Generates a csv file named "myfile.csv" for the unit heat with R = 2.25, in the x-y domain [-2,2]x[-2,2] for Nx=Ny=30 (default: grid 30x30 -> 900 points). After the file generation, a scatter plot is shown on screen with the generated flow distribution (blue = 0, green=1). 

Note: The heat distribution function used in this script is exactly the one displayed in Equation (5) of the description. For a circular heat pattern however (with center (0,0) and radius R), R must be squared (in this case, we should uncomment line 98 and comment line 99 of the heat_generator.py file).

### Answers for the exercise questions
#### Exercise 1.
##### Q. Describe how the particles are organized in the various objects.
**A**. 
The class `Particle` has three child classes `PingPongBall`, `Planet`, and `MaterialPoint`. Particle class contains general purpose variables as members, common to all types of particles, namely `position`, `velocity`, `force`, `mass`. The daughter classes contain additional members that are cpecific to each particle type. For instance, `PingPongBall` defines `radius` of the balls and `contact dissipation`, with the associated methods, `getRadius`, `getContactDissipation` etc.
Similarly `Planet` defines `name` and `MaterialPoint` heat-related properties of the particles, such as `temperature` and `heat_rate`.

The class `ParticlesFactoryInterface` has three child classes `PingPongBallsFactory`, `PlanetsFactory`, and `MaterialPointsFactory`.
Each child class sets up a simulation of the system of corresponding objects such as ping-pong balls, planets and material points. 

The class `Matrix` is a class that allows the manipulation of matrix(2D data) more easily.

The class `FFT` is a class that helps computing the data in matrix format, especially the 2D Discrete Fourier Transform and frequencies.

#### Exercise 4-5. 
##### Q. Explain how you integrate the boundary condition within the existing code.
**A**. 

To integrate this boundary condition, we added a new member in the class `MaterialPoint` of boolean type named `isBoundary`, which is set equal to 1 for boundary points and 0 elsewhere. Then, temperatures of points are updated in every step by `compute_temperatures` only if the point is not in the boundaries. 

This process simply does not apply the temperature evolution in the boundaries. However, it does not affect the adjacent points in any other way.  

The setting of the `isBoundary` variables of the points is done based on their coordinates (see the implementation of the setting in `test_heat`). For the implementation in the whole project, the values of `isBounday` should appear in the input file, like `temperature` and `heat_rate`. 

#### Exercise 4-6.
##### Q. Describe how to launch a simulation which will produce dumps observable with Paraview for a grid of 512 × 512 particles.
**A**.

To initiate a simulation for material points, at first we need to define all the necessary inputs (parse in the main file). Since we still have not been taught how to use the argument parser of python with the c++ compiler, we assume that for now the input arguments can be directly set in the main.cc . 

We need to specify: 
- `nsteps`   : Number of simulation steps to run
- `freq`     : Frequency of dumping of the particles' state in the output files (e.g. every 10 steps)
- `filename` : The name of the input file, which will be a csv file of the format [x y z ... temperature heat_rate] 
- `type`     : Set to "material_point"

Moreover, all parameters which are specific for the "material
points" type of particles have to be set in the `material_points_factory` (see lines 34-37). 

Regarding the input file, for the case of question 4-5, we can generate it by using the file `heat_generator.py`, after specifying the functions `heat_function` and `init_temperature` according to the simulation that we need to run. In this point we have to make sure that the format of the input file can be correctly read by the `.read()` method of the `CsvReader` class that we use in our project. Since every line of the file is used for the particle creation, we need to make sure that the assignment of the values in the file is done correctly to the respective members of the class `material_point`.  

To generate a 512x512 grid of particles by using the `heat_generator.py`, we need to set N=512, e.g. for the heat rate function of Eq. (5) and initial homogeneouos temperature equal to 1: 

``` commandline
$ ./heat_generator.py -R 0.5 -x -1 1 -y -1 1 -Nx 512 -Ny 512 
```     
The produced file will be of the form: 

<x, y, temperature, heat_rate, isBoundary> 

After the end of the execution, the output files for every particle will be generated with the evolution of their temperature over time. The files can be then used for an animation in Paraview, where temperature values can be visualized by using different colors.
