# Homework 4. Pybind and Trajectory Optimization

### Students 
Sohyeong Kim, Dimitrios Tsitsokas
 
## How to build and execute C++ files
### Building
``` commandline
$ mkdir build && cd build
$ cmake .. -DCMAKE_BUILD_TYPE=Release
$ make
```
When compiled successfully, the execution python files as well as init.csv file will be copied to `Homework4/build/`.
Make sure to execute python files in this directory. 


### How to execute the optimization routine (Exercise 7) 
The optimization is performed by the main function of file `optimizer.py`. 
In order to run an optimization from the command line the following syntax is required:

``` commandline
$ python3 optimizer.py -p [planet_name] -dir [directory_of_traj_to_test] -dir_r [directory_of_reference_traj] -f [input_filename] -s [scaling_factor] --acc_tol [acc_tolerance_scale] --save_plot_as [figure_filename]
```


##### Arguments list 
* `-p/--planet` is the name of the planet for which trajectory error is to be calculated
* `-dir/--directory` is the directory where the generated trajectory files are stored, e.g. [-dir  ./dumps/]
* `-dir_r/--directory_ref` is the directory where the reference trajectory files are stored, e.g. [-dir_r ./trajectories/]
* `-f/--filename` is the name of the input file, e.g. [-f init.csv] (optional, default= init.csv) 
* `-s/--scale` is the value of the scaling factor applied, e.g. [-s 0.6] (optional, default= 1.0) 
* `--acc_tol` is the accepted tolerance for the scale values, e.g. [--acc_tol 1e-3] (optional, default= 1e-6)
* `-s_as/--save_plot_as` indicates the name for saving the plot into file, e.g. [-s_as my_figure] (optional, by default plot is not saved). 


#### Example of execution 
To optimize scaling factor for planet mercury, starting from value 0.8, with input file 'init.csv', with tolerance 1e-4 we should type:
``` commandline
$ python3 optimizer.py -p mercury -dir ./dumps/ -dir_r ./trajectories/ -f init.csv -s 0.8 --acc_tol 1e-6 -s_as my_figure.png
```
The results of the optimization are printed on screen, together with the plot showing the values of scores-error at every step. In the above, the figure is saved in file my_figure.png.

For the case of mercury, the optimal value of the scaling factor is found equal to 0.388997, hence the correct initial velocity should be {Vx Vy Vz] = [0.018595707825983812 -0.006781314758978278 -0.0022606932285429465].

### Answers for the exercise questions
#### Exercise 1.
##### Q. In class ParticlesFactory, `createSimulation` function has been overloaded to take functor as one of its argument. Comment on what this function is doing?
**A**. The `createSimulation` function is overloaded to take a functor as an argument.
By doing this, the user can specify additional parameters for the compute object and thus customize the computation of the system evolution. 
It can be used for setting the different compute types, such as the conductivity and heat capacity using `ComputeTemperature`. 
When no custom function is specified, the compute object will be initialized with default values. 


#### Exercise 2. 
##### Q. How will you ensure that references to Compute objects type are correctly managed in the python bindings?
**A**. In class SystemEvolution, `addCompute` function takes an argument of a type Compute in a shared pointer, 
therefore, we have to specify a template type that denotes a _special holder type_ to manage the reference to the object accordingly. 
In this case, we wrapped "Compute" objects into their designated holder types as `std::shared_ptr<...>` in the python binding. 
More details can be found in [Smart pointers](https://pybind11.readthedocs.io/en/stable/advanced/smart_ptrs.html) section of the pybind11 documentation.

#### Exercise 4. 

We run the file `main.py` to perform a simulation of the planets movement in 365 days. To do this, from the command line we type: 

``` commandline 
$ python3 main.py 365 1 init.csv planet 1 
```

This is a prompt for running particles code for 365 steps, with dump frequency of 1 step, by using starting point of file `init.csv`, 
for particle type 'planet' and for time steps of 1 day (arguments in order they are given as inputs). 
The produced module `pypart` needs to be in the same directory as `main.py`. 

The trajectory files generated from the simulation will be stored in a newly created folder, named `\dumps`, 
which is in the same directory as `main.py`. 

Indeed, we observe that, apart from planet 'mercury', all planet trajectories are very close to those found in the directory `trajectories`, at the corresponding days. 
In the following exercises, we create routines to scale the initial magnitude of velocity of mercury, 
to minimize the observed aggregated error, by using `trajectories` directory files as reference. 

#### Exercise 5. 

The two functions are created and stored in file `simulation_error.py`. 
To test their functionality, a `main` function is created in the same file, which calculates the trajectory error, 
for a specific planet, from the trajectory files generated, compared to a reference set of trajectories, considered correct. 
The simulation error is printed on screen. Execution details are as follow:

##### Execution of python code `simulation_error.py` 
```commandline 
$ python3 simulation_error.py --planet [planet_name]  --directory [directory_of_traj_to_test] --directory_ref [directory_of_reference_traj]
```

##### Arguments list 
* `--planet` is the name of the planet for which trajectory error is to be calculated
* `--directory` is the directory where the generated trajectory files are stored, e.g. [--directory  ./dumps/]
* `--directory_ref` is the directory where the reference trajectory files are stored, e.g. [--directory_ref ./trajectories/] 


##### Example
For the integral error of mercury from trajectory files stored in ./dumps/ compared to reference trajectory files stored in ./trajectories/ we should type: 
```
$ python3 simulation_error.py --planet mercury --directory ./dumps/ --directory_ref ./trajectories/
```

#### Exercise 6. 

The three requested functions are stored in file `generate_input_launch.py`. A main function is created to test their functionality. 
It can be executed to calculate the integral error of a simulation after scaling the initial velocity of a planet by using a given scale factor. 
Execution details are as follow:

##### Execution of python code `generate_input_launch.py`
```commandline 
$ python3 generate_input_launch.py -p [planet_name] -dir [directory_of_traj_to_test] -dir_r [directory_of_reference_traj] -f [input_filename] -s [scaling_factor]
```

##### Arguments list 
* `-p/--planet` is the name of the planet for which trajectory error is to be calculated
* `-dir/--directory` is the directory where the generated trajectory files are stored, e.g. [-dir  ./dumps/]
* `-dir_r/--directory_ref` is the directory where the reference trajectory files are stored, e.g. [-dir_r ./trajectories/]
* `-f/--filename` is the name of the input file, e.g. [-f init.csv] (optional, default= init.csv) 
* `-s/--scale` is the value of the scaling factor applied, e.g. [-s 0.6] (optional, default= 1.0) 

#### Example 
For mercury and scale 0.8 we should type: 

```commandline 
$ python3 generate_input_launch.py -p mercury -dir ./dumps/ -dir_r ./trajectories/ -f init.csv -s 0.8
```
   
