# SP4E-Group LUTS

This repository is for the assignments of the course **Scientific Programming for Engineers(MATH-611)** in FALL 2020. 

The maintainers of this repository is **Sohyeong Kim** and **Dimitrios Tsitsokas**. 

You can find our work for each assignment in separate folders. 

| HW #    | Folder                            | Date |
| :---: | :---: | :---: |
| 1             | Assignment01_Conjugate_Gradient   |  14/10/2020|
| 2             | Homework2  | 28/10/2020 | 
| 3             | Homework3  |  09/12/2020|
| 4             | Homework4  |  18/01/2021|

If you have any question, please contact us.
* sohyeong.kim@epfl.ch
* dimitrios.tsitsokas@epfl.ch
