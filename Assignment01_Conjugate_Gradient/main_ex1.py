import numpy as np
import argparse
import sys
from optimizer import scipy_optimizer
from function import show_plots

parser = argparse.ArgumentParser('[SP4E] Homework1 >> Exercise1')
parser.add_argument('--x', nargs=2, default= (2,3), type=float, metavar=('x', 'y'), help='An initial input vector X = (x,y) where x and y are floats. Default is X=(2,3)')
parser.add_argument('--solver', type=str, required=True, metavar='[ BFGS | CG ]', help='Type of solver for the scipy optimizer. Options: BFGS | CG ')
parser.add_argument('--show_plot', action='store_true', default=False, help='Show the plot of the iteration by setting this option.')
args = parser.parse_args()

# Parse the arguments and handle the wrong argument
SOLVER = args.solver
if (SOLVER != 'BFGS') & (SOLVER != 'CG'): sys.exit("The solver argument should be either BFGS or CG")
X = args.x
SHOW_PLOT = args.show_plot

# Set coefficients for the quadratic function
A = np.array([[4, 0], [1, 3]])
b = np.array([[0], [1]])
print('------------------ Exercise 01 ---------------------')
print('The quadratic function S(x) = x^T * A * x - x^T * b')
print('where A = {} and b ={}'.format(A, b))
print('Initial input vector x = [{:.3f}, {:.3f}]'.format(X[0], X[1]))
print('----------------------------------------------------')
print('The chosen solver is < {} >'.format('Conjugate Gradient' if SOLVER == 'CG' else SOLVER))


# A dictionary for saving the result of the steps of each solver.
iter_res_all={}

# Optimize the function with chosen solver
res, iter_res = scipy_optimizer(X, 2*A, b, SOLVER)

# Store the iteration results
iter_res_all['Scipy {}'.format('Conjugate Gradient' if SOLVER == 'CG' else SOLVER)] = iter_res

# Print out the optimized result
print(res.message)
print('The x value minimizing the quadratic function: x = [{:.3f}, {:.3f}]'.format(res.x[0], res.x[1]))

# Plot the S(x) and the solution at each iteration step
# when 'show_plot' flag is activated
if SHOW_PLOT:
    show_plots(iter_res_all, 'Exercise 01', 2*A, b)

