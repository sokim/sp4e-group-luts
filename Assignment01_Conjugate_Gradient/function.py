import numpy as np
import matplotlib.pyplot as plt
import os
import sys

def quadratic_function_S(x, A, b):
    '''
        A quadratic function to minimize.
        S(x) = (1/2) * x^T * A * x - x^T * b

    :param x: Input to the equation (2D column vector (x,y))
    :param A: The first coefficient of the equation (2x2 matrix)
    :param b: The second coefficient of the equation (2x1 matrix)
    :return: Result of the equation.
    '''

    S_x = (1/2)*np.dot(np.dot(x.T, A), x) - np.dot(x.T,b)
    S_x = S_x.flatten()[0] # to make the result as a float
    return S_x


def construct_coefficient(input_file_directory):
    '''
        Reads the text file contents as an array.
        It will return the error message if there is no file exists or the contents are not in a matrix format.
    :param input_file_directory: The path to the file to read
    :return: An numpy array.
    '''
    try:
        val = np.loadtxt('./' + input_file_directory)
        return val
    except:
        if not os.path.isfile(input_file_directory):
            sys.exit("[Input File Directory Error] Could not find a file [{}]. Please check the file path.".format(input_file_directory))
        else:
            sys.exit("[Input File Value Error] Check the coefficient matrix in [{}].".format(input_file_directory))
            

def plot_function_Sxy(ax, A, b, x_min, x_max, y_min, y_max):
    '''
        Function to plot S(x) = (1/2) * x^T * A * x - x^T * b on existing figure and 3-D axes
        in ranges of x and y that include all optimizer steps
    '''
    X_points = np.arange(x_min, x_max, 0.5)
    Y_points = np.arange(y_min, y_max, 0.5)
    X_points_mg, Y_points_mg = np.meshgrid(X_points, Y_points)

    S_XY =[]
    for x, y in zip(X_points_mg.flatten(), Y_points_mg.flatten()):
        a1 = np.array([[x], [y]])
        S_XY.append(quadratic_function_S(a1, A, b))
    Z_points = np.resize(np.asarray(S_XY), X_points_mg.shape)

    ax.plot_surface(X_points_mg, Y_points_mg, Z_points, cmap='viridis', alpha=0.9)
    ax.set_axisbelow(True)

def show_plots(iter_res_all, fig_title, A, b):
    '''
        Function to plot the iteration steps of the minimization.
    :param iter_res_all: A dictionary which stores the results of the iteration for different solvers.
                         For example >> iter_res_all ={'SOLVER1': [[x values], [y values], [S(x,y) values]],
                                                        'SOLVER2': [[x values], [y values], [S(x,y) values]], ...}
    :param fig_title: The title of the figure (str).
    :param A: The first coefficient of the objective quadratic function S(x) = (1/2) * x^T * A * x - x^T * b
    :param b: The second coefficient of the objective quadratic function S(x) = (1/2) * x^T * A * x - x^T * b
    '''
    fig = plt.figure(figsize=plt.figaspect(0.3))
    fig.suptitle(fig_title, fontsize=16, fontweight='bold')

    # Find the absolute maximum value among x and y to be used for plotting the S(x) surface.
    x_values = [[] + iter_res_all[dict_key][0] for dict_key in iter_res_all.keys()][0]
    y_values = [[] + iter_res_all[dict_key][1] for dict_key in iter_res_all.keys()][0]
    abs_max_value = max(x_values + y_values, key=abs)

    for i, solver_type in enumerate(iter_res_all.keys()):
        ax = fig.add_subplot(1,len(iter_res_all.keys()), i+1, projection='3d')

        # Plot the S(x)
        plot_function_Sxy(ax, A,b,
                          round(-abs_max_value-1), round(abs_max_value+1),
                          round(-abs_max_value-1), round(abs_max_value+1))

        # Unpack the iteration result
        iteration_res_x = iter_res_all[solver_type][0]
        iteration_res_y = iter_res_all[solver_type][1]
        iteration_res_Sx = iter_res_all[solver_type][2]

        # Draw the 3D plot of the iteration steps
        ax.set_xlabel('x', fontsize=12)
        ax.set_ylabel('y', fontsize=12)
        ax.set_zlabel('$S([x,y]^T)$', fontsize=12)
        ax.set_title(solver_type, fontsize=12, fontweight='bold')
        ax.set_title(solver_type+'\n $x^{*}$' +' = [{:.3f},{:.3f}]'.format(iteration_res_x[-1], iteration_res_y[-1]),
                     fontsize=16, fontweight='bold')

        ax.plot(iteration_res_x, iteration_res_y, iteration_res_Sx,
                marker='o', linestyle='--', color='r', label ='Optimizer steps')

    plt.tight_layout()
    plt.show()
    
    
def check_symmetric(a, rtol=1e-05, atol=1e-08):
    '''
        A function to check if 2-dim matrix a is symmetric by 
        comparing it to its transpose

    :param a: The 2-dim matrix to check symmetry
    :param rtol: (otpinal) relative tolerence for np.allclose (default 1e-05) 
    :param atol: (optional) absolute tolerence for np.allclose (default 1e-08)
    :return: Bool (True if a is symmetric, False if a not symmetric)
    '''
    return np.allclose(a, a.T, rtol=rtol, atol=atol)