## Homework 1. Conjugate_Gradient


### Students 
Sohyeong Kim, Dimitrios Tsitsokas

### Overview
    .
    ├── main_ex1.py                   # Main execution script for exercise 1
    ├── main_ex2.py                   # Main execution script for exercise 2
    ├── optimizer.py                  # Contains a function 'scipy_optimizer' which returns the result of the minimization problem using scipy solvers.
    ├── conjugate_gradient.py         # Contains a function 'conjugate_gradient' which returns the result of the minimization problem using our CG implementation.
    ├── function.py                   # Contains several useful functions for this homework. 
    ├── A.txt                         # txt file for coefficient A in qudratic function S(x). 
    ├── b.txt                         # txt file for coefficient b in qudratic function S(x). 
    └── README.md
    
### Dependencies
* Numpy 1.19.1
* Scipy 1.5.2
* Matplotlib 3.3.2

### How to execute the codes
#### Exercise 1. Scipy Optimization
```
python main_ex1.py --solver CG --show_plot --x 2 3 
```

* `--solver SOLVER` to indicate which solver to use for the optimization. The SOLVER is either BFGS or CG. 
* (Optional) `--show_plot` to show the plot of the optimization.
* (Optional) `--x x0 y0` are the initial x, y value of the 2D vector x. (Default x=(2,3))


#### Exercise 2. Conjugate Gradient
```
python main_ex2.py --my_solver --scipy_CG --scipy_BFGS --show_plot --coef_A A.txt --coef_b b.txt --x 2 3
```

* Set `--my_solver` to use conjugate gradient solver implemented by ourselves. 
* Set `--scipy_CG` to use conjugate gradient solver from Scipy. 
* Set `--scipy_BFGS` to use BFGS solver from Scipy. 
* (Optional) Set `--show_plot` to show the plots of the optimization by chosen solver.
* (Optional) `--x x0 y0` are the initial x, y value of the 2D vector x. (Default x=(2,3))
* (Optional) `--coef_A` the path to the text file of coefficient A. (Default file path is './A.txt')
* (Optional) `--coef_b` the path to the text file of coefficient b. (Default file path is './b.txt')

    (_See below for creating coefficient txt files_)

##### Creating coefficient files 
Coefficient files should only contain the elements of the matrices A and b separated by spaces. 
Rows of the files correspond to rows of the matrices. 
For example, see the included files 'A.txt' and 'b.txt'. 

**Attention** should be given to the fact that the matrix A for _Exercise 2_ is used to minimize the function stated at the beginning of _Exercise 2_. 

Hence, in case we wanted to find the minimum of the function of _Exercise 1_, 
matrix A must be adjusted to match the expression in _Exercise 2_ 
(i.e. A of Ex. 1 must be multiplied by 2 before given as input to Ex. 2). 
However, in this case, since A of _Exercise 1_ is not symmetric, the code of _Exercise 2_ will stop and after printing an error message. 

