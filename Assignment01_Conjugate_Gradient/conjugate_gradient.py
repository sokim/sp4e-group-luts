import numpy as np
from function import quadratic_function_S

# function applying CG
def conjugate_gradient(x0, A, b, max_iter = 1000, acc_tolerance = 1e-6):
    '''

    :param x0: The initial 2D input vector to the function S(x).
    :param A: The first coefficient of the equation (2x2 matrix)
    :param b: The second coefficient of the equation (2x1 matrix)
    :param max_iter: The maximum iteration for running the solver (default = 1000)
    :param acc_tolerance: Tolerance for termination. (default = 1e-6)
    :return:
        res: The optimization result object.
        iter_res: The result at each iteration step during the optimization. [[x], [y], [S(x,y)]]
    '''
    def store_iteration_result(res):
        '''
            The Callback function for the scipy.optimize.minimize.
            Append the results X =(x,y) and S(X) of each step to lists.
        '''
        x, y = res
        Sx = quadratic_function_S(np.array([[x],[y]]), A, b)
        iteration_res_x.append(x)
        iteration_res_y.append(y)
        iteration_res_Sx.append(Sx)

    # Arrange input as a 2D column vector
    input_x, input_y = x0
    X =  np.asarray(x0)

    # Create a list to store the iteration result
    iteration_res_x = [input_x]
    iteration_res_y = [input_y]
    iteration_res_Sx = [quadratic_function_S(X, A, b)]

    # Optimize the objective function
    i = 0 # iteration step
    r = b - np.einsum('ik,k->i', A, X)
    p = r
    dist = np.einsum('i,i', r, r)
    #rsnew = rsold
    while (i < max_iter) and (np.all(dist > acc_tolerance)):
        Ap = np.einsum('ik,k->i', A, p)
        alpha = np.einsum('k,k->', r, p)/(np.einsum('i,i->', p, Ap))
        X = X + alpha*p

        # Store the iteration steps
        store_iteration_result(X.flatten())

        # Find new direction
        r = b - np.einsum('ik,k->i', A, X)
        beta = (r * Ap) / (np.einsum('i,i->', p, Ap))
        p = r - beta * p

        dist = np.einsum('i,i', r, r)

        # Update count
        i +=1

    # Pack the iteration results
    iter_res = [iteration_res_x, iteration_res_y, iteration_res_Sx]

    # Optimization result
    res = X.flatten()

    return res, iter_res

