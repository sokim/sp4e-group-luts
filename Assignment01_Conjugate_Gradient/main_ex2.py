import argparse
import sys
from optimizer import scipy_optimizer
from conjugate_gradient import conjugate_gradient
from function import show_plots, construct_coefficient, check_symmetric

parser = argparse.ArgumentParser('[SP4E] Homework1 >> Exercise2')
parser.add_argument('--my_solver', action='store_true', default=False, help='Use my solver to minimize the function.')
parser.add_argument('--scipy_CG', action='store_true', default=False, help='Use Scipy Conjugate Gradient solver to minimize the function.')
parser.add_argument('--scipy_BFGS', action='store_true', default=False, help='Use Scipy BFGS solver to minimize the function.')
parser.add_argument('--show_plot', action='store_true', default=False, help='Show the plot of the iteration by setting this option.')
parser.add_argument('--coef_A', type=str, default='./A.txt', help='The text file storing the coefficient A')
parser.add_argument('--coef_b', type=str, default='./b.txt', help='The text file storing the coefficient b')
parser.add_argument('--x', nargs=2, type=float, default= (2, 3), metavar=('x', 'y'), help='An input vector X = (x,y) where x and y are floats. Default is X=(2,3)')
args = parser.parse_args()

# parse the arguments
SOLVER_TYPE = []
if args.my_solver: SOLVER_TYPE.append('mine')
if args.scipy_CG: SOLVER_TYPE.append('CG')
if args.scipy_BFGS: SOLVER_TYPE.append('BFGS')

if len(SOLVER_TYPE) == 0:
    sys.exit('[Argument Error] None of the solver is chosen. '
                     'Please set at least one solver: [ --my_solver / --scipy_CG / --scipy_BFGS ]')

A = construct_coefficient(args.coef_A)
b = construct_coefficient(args.coef_b)
X = args.x
SHOW_PLOT = args.show_plot

# Handling Errors of the coefficients
#   1) Size of A and b should match 
if not A.shape[1]==b.shape[0] :
    sys.exit('[Input Value Error]: Dimensions of A and b are not compatible. '
             'Number of columns of A should be equal to number of rows of b.')

#  1b) For this Exercise, the only accepted way is 2x2 for A and 2x1 for b as x is also 2x1 since x is also defined as 2x1 in arguments)
if not A.shape[1]== 2 or not b.shape[0]==2: 
    sys.exit('[Input Value Error] Dimensions of A or/and x are not correct.'
             ' A should be [2x2] matrix and b [2x1] array') 

#   2) Non-symetric matrix A
if not check_symmetric(A):
    sys.exit('[Input Value Error] Matrix A is not symmetric. '
                     'Please update text file for A with a symmetric matrix.')


# Set coefficients for the quadratic function
print('------------------ Exercise 02 ---------------------')
print('The quadratic function S(x) = 1/2 * x^T * A * x - x^T * b')
print('where A = {} and b ={}'.format(A, b))
print('Initial input vector x = [{:.3f}, {:.3f}]'.format(X[0], X[1]))
print('----------------------------------------------------')

# A dictionary for saving the result of the steps of each solver.
iter_res_all={}

if 'mine' in SOLVER_TYPE:
    print('The chosen solver is < My Conjugate gradient >')

    # Optimize the function with chosen solver
    res, iter_res = conjugate_gradient(X, A, b)

    # Store the iteration results
    iter_res_all['My Conjugate Gradient'] = iter_res

    # Print out the optimized result
    print('The x value minimizing the quadratic function: x = [{:.3f}, {:.3f}]'.format(res[0], res[1]))
    print('----------------------------------------------------')

if 'CG' in SOLVER_TYPE:
    print('The chosen solver is < Scipy Conjugate gradient >')

    # Optimize the function with chosen solver
    res, iter_res = scipy_optimizer(X, A, b, 'CG')

    # Store the iteration results
    iter_res_all['Scipy Conjugate Gradient'] = iter_res

    # Print out the optimized result
    print('The x value minimizing the quadratic function: x = [{:.3f}, {:.3f}]'.format(res.x[0], res.x[1]))
    print('----------------------------------------------------')

if 'BFGS' in SOLVER_TYPE:
    print('The chosen solver is < Scipy BFGS >')

    # Optimize the function with chosen solver
    res, iter_res = scipy_optimizer(X, A, b, 'BFGS')

    # Store the iteration results
    iter_res_all['Scipy BFGS'] = iter_res

    # Print out the optimized result
    print('The x value minimizing the quadratic function: x = [{:.3f}, {:.3f}]'.format(res.x[0], res.x[1]))
    print('----------------------------------------------------')

# Plot the S(x) and the solution at each iteration step
# when 'show_plot' flag is activated
if SHOW_PLOT:
    show_plots(iter_res_all, '[Exercise 02]', A, b)

