import numpy as np
from scipy.optimize import minimize
from function import quadratic_function_S

def scipy_optimizer(x0, A, b, solver, acc_tolerance = 1e-6):
    '''
        A function that uses Scipy.optimize.minimize to optimize the given quadratic function S(x).

    :param x0: The initial 2D input vector to the function S(x).
    :param A: The first coefficient of the equation (2x2 matrix)
    :param b: The second coefficient of the equation (2x1 matrix)
    :param solver: The type of solver (str)
    :param acc_tolerance: Tolerance for termination. Default = 1e-6
    :return:
        res: The optimization result object.
        iter_res: The result at each iteration step during the optimization. [[x], [y], [S(x,y)]]
    '''

    def store_iteration_result(res):
        '''
            The Callback function for the scipy.optimize.minimize.
            Append the results X =(x,y) and S(X) of each step to lists.
        '''
        x, y = res
        Sx = quadratic_function_S(np.array([[x],[y]]), A, b)
        iteration_res_x.append(x)
        iteration_res_y.append(y)
        iteration_res_Sx.append(Sx)

    # Arrange input as an array
    X = np.asarray(x0)

    # Create a list to store the iteration result
    iteration_res_x = [x0[0]]
    iteration_res_y = [x0[1]]
    iteration_res_Sx = [quadratic_function_S(X, A, b)]

    # Solve the minimization problem
    res = minimize(quadratic_function_S, x0=X, args=(A,b), method=solver, callback= store_iteration_result,tol=acc_tolerance)

    # Pack the iteration results
    iter_res = [iteration_res_x, iteration_res_y, iteration_res_Sx]

    return res, iter_res
