# Homework 2. C++ Classes

### Students 
Sohyeong Kim, Dimitrios Tsitsokas

## Overview
    .
    ├── src
    |   ├── main.cpp                           # Main execution script for exercise 2.
    |   ├── series.cpp                         # Implementation of the Series Class.
    |   ├── series.h
    |   ├── compute_arithmetic.cpp             # Implementation of the ComputeArithmetic Class.
    |   ├── compute_arithmetic.h
    |   ├── compute_pi.cpp                     # Implementation of the ComputePi Class.
    |   ├── compute_pi.h
    |   ├── dumper_series.cpp                  # Implementation of the DumperSeries Class. 
    |   ├── dumper_series.h
    |   ├── print_series.cpp                   # Implementation of the PrintSeries Class. 
    |   ├── print_series.h
    |   ├── write_series.cpp                   # Implementation of the WriteSeries Class. 
    |   ├── write_series.h 
    |   ├── argument_parser.cpp                # The helper function for handling the input arguments.
    |   ├── argument_parser.h
    |   ├── CMakeLists.txt                     # CMakeLists file to add the executable files.
    |   └── file_read.py	                   # Python script to read output file of series and plot results
    ├── CMakeLists.txt
    ├── .gitignore
    └── README.md
   
The diagram below shows the relationship between each classes implemented for this program.

![](./src_file_structure.jpeg)

    
    
## How to build and execute
### Building
``` commandline
$ mkdir build && cd build
$ cmake .. -DCMAKE_BUILD_TYPE=Release
$ make
```

### Execution via command line:
When compiled successfully, the main execution file will be created in `homework2/build/src/`.

``` commandline
$ ./main --series_type [arithmetic | pi] --max_iter [<unsigned int>] --frequency [<unsigned int>] --output_type [ screen | file] --filename [<output_filename>] --separator [ space | comma | pipe ]  --precision [<unsigned int>] 
```

##### Arguments list 
* `--series_type` is the type of series that you would want to compute. Options are: [pi | arithmetic] (required)
* `--max_iter` is the number N of iterations (terms) that will compose the series. (default = 10)
* `--frequency` is the  step between printed terms of the series. (default = 1)
* `--output_type` is the wished output type for the user. Options are [file | screen]. (default = screen) 
* `--filename` is the wished name of the output file if output_type is set to file. (default = output) 
* `--separator` is the wished separator which dictates the file type. Options are [ space | tab | comma | pipe ] for .txt, .txt, .csv or .psv file, repsectively. (default = space)
* `--precision` is the wished precision of the values in the scientific output in number of digits (unsigned integer). (default = 10) 
* `--help` will print out the argument options for the execution file.

### Execution of python code file_read.py 
This python file is located in `homework2/src`.

``` commandline
$ ./file_read.py --filename [<filename>] --save_as [<figure_filename>]
```

- By default, `file_read.py` will search for the file in the same directory. 
{- Therefore, it is highly recommended to explictly indicate the file path to the argument. -}
- The output figure will be created in the same folder as `file_read.py` by default.

##### Arguments list 
* `--filename` is the name of the file (.txt) that needs to be read by file_read.py 
* `--save_as` indicates that the produced figure needs to be saved in a file named <figure_filename> (optional)


## Execution examples
#### 1. Examples of execution with full argument list 
``` commandline
$ ./main --series_type pi --max_iter 100 --frequency 10 --output_type file --filename my_output --separator comma  --precision 5 
```

The above example generates pi series with N = 100 terms and frequency 10, in file named "my_output.csv" with precision 5 (scientific format always).

#### 2. Examples of execution with partial argument specification (unspecified arguments take default values) 

##### 2-1. Print on the screen: 

``` commandline
$ ./main --series_type arithmetic
```
The above prints the arithmetic series with the default arguments (N = 10, frequency = 1, output on screen, precision 10) 

##### 2-2. Print in file

``` commandline
$ ./main --series_type pi --output_type file 
```
The above prints the pi series in file with the default arguments (N = 10, frequency = 1, file name = "output.txt", precision 10)

``` commandline
$ ./main --series_type pi --max_iter 100 --frequency 10 --output_type file --filename my_output  
```

The above prints the pi series with N = 100 terms and frequency 10 in file named "my_output.txt" (default precision 10) 

``` commandline
$ ./main --series_type arithmetic --max_iter 100 --frequency 10 --output_type file --separator pipe   
```

The above prints the arithmetic series with N = 100 terms and frequency 10 in file named "output.psv" (output by default and the ending .psv dictated by the pipe separator).

#### 3. Examples of execution of python code file_read.py  
``` commandline
./file_read.py --filename ../build/src/my_output.txt --save_as my_figure
```
The above example reads file named `my_output.txt` in the `homework2/build/src` directory
and saves the produced plot in file my_figure.png in `homework2/src`. 
In case the file is not in the folder src, the full path must be specified as shown in the example.

## Comments
#### Exercise 3.7
While we could have `,` and `|` as arguments in the commandline, having ` ` or `\t` was not easy,
and it might confusing for user whether they have correctly set the options.
Therefore, we have decided to have concrete name for each options, for example,
 
 * ` ` to `space`
 * `\t` to `tab`
 * `,` to `comma`
 * `|` to `pipe`.

#### Exercise 3.10
We have written a simple input parser. The source files are `argument_parser.h` and `argument_parser.cpp`.
This parser check if argument is provided with right name and with values and store the those values accordingly.
This parser takes all the values as `std::string` and the correctness of the values is checked not yet checked. 

#### Exercise 4
The dump() function of PrintSeries and WriteSeries classes is implemented 
both with simple std::cout and with std::ostream according to Exercise 4. 
The default to be used is the latter. 
To check the functionality of the implementation with simple std::cout, 
please uncomment line 72 (`dumper_object->dump();`) of `main.cpp` and comment line 73 (`*output << *dumper_object << std::endl;`).

### Answers for the exercise questions
#### Exercise 2. Series class
##### Q. What is the best way/strategy to divide the work among yourselves?
**A**. The work has been divided into building each classes, Series class and Dumper class. 
So, we can work simultaneously in the beginning, for instance, one was implementing Series class family 
while one is working on implementing Dumper class family. 

#### Exercise 5. Series complexity
##### Evaluate the global complexity of your program.
**A**. Although we have implemented our program having `current_value` and `current_index` members in the Series class from the beginning of our implementation, 
if we were naively implemented `compute` function using for-loop in the Series class, 
**the complexity of our program should be of order $`N^2`$**, where $`N`$ is the number of terms in the sum. 
For example, if $`N=3`$, the number of operation in the `compute` function is $`N + (N-1) + (N-2)`$, which is the order of $`N^2`$, 
because the entire series will be recomputed each time it is called by the **DumperSeries**. 

##### Q. How is the complexity now?
**A**. Having the `current_value` and `current_index` as a members in the Series class allow us to reduce the complexity to order $`N`$, 
since the sum does not need to be re-computed at each call of the `compute` function.

##### Q. In the case you want to reduce the rounding errors over floating point operation by summing terms reversely, what is the best complexity you can achieve?
**A**. Assuming that we keep the `current_value` and `current_index` as a members in the Series class, 
summing the terms reversely would not change the complexity of the program 
meaning that the complexity would still be of order $`N`$.   

