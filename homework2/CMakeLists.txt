cmake_minimum_required(VERSION 2.6)

# project name and language
project(HW2-Cpp_Classes LANGUAGES CXX)

# Set cpp standards
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

# Directory that contains executable files
add_subdirectory(src)


