//
// Created by Sohyeong Kim on 23.10.20.
//

#ifndef HW2_CPP_CLASSES_COMPUTE_PI_H
#define HW2_CPP_CLASSES_COMPUTE_PI_H

#include "series.h"

class ComputePi : public Series{
public:
    // Constructor
    ComputePi();

    double compute(unsigned int N) override;
    double getAnalyticPrediction() override;

    // Destructor
    ~ComputePi();
};


#endif //HW2_CPP_CLASSES_COMPUTE_PI_H
