//
// Created by dimitris on 23.10.20.
//

#ifndef SP4E_GROUP_LUTS_DUMPER_SERIES_H
#define SP4E_GROUP_LUTS_DUMPER_SERIES_H

#include "series.h"
#include <iostream>
//#include <memory>

class DumperSeries{

public:
    //Constructor
    DumperSeries(Series &series);

    virtual void dump(std::ostream &os) = 0; //dump function using std:ostream (ex4)
    virtual void dump() =0; //dump function using std:cout
    virtual void setPrecision(unsigned int precision);

    std::string filename;

    //Destructor
    virtual ~DumperSeries();

protected:
    Series &series;
    unsigned int precision;
 };

inline std::ostream &operator<<(std::ostream &stream, DumperSeries &_this) {
    _this.dump(stream);
    return stream;
}
#endif //SP4E_GROUP_LUTS_DUMPER_SERIES_H
