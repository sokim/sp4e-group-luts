//
// Created by dimitris on 24.10.20.
//

#ifndef SP4E_GROUP_LUTS_WRITE_SERIES_H
#define SP4E_GROUP_LUTS_WRITE_SERIES_H

#include "dumper_series.h"
#include <iostream>

class WriteSeries : public DumperSeries{
public:
    WriteSeries(Series &series, int frequency, int maxiter, std::string separator, std::string filename); //Constructor

    void dump(std::ostream &os) override; //dump function using std:ostream (ex4)
    void dump() override; //dump function using std:cout
    void setSeparator(std::string sep);

    ~WriteSeries(); //Destructor

protected:
    int frequency;
    int maxiter;
    std::string separator;
};


#endif //SP4E_GROUP_LUTS_WRITE_SERIES_H
