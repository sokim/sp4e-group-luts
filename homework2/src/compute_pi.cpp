//
// Created by Sohyeong Kim on 23.10.20.
//

#include "compute_pi.h"
#include <cmath>

ComputePi::ComputePi(){
    this->computeTerm = [](unsigned int k){return 1. / (1. * k * k);};
}

double ComputePi::compute(unsigned int N) {
    Series::compute(N);
    return sqrt(6. * current_value);
}

double ComputePi::getAnalyticPrediction() {
    return M_PI;
    /* Alternative way using asin() */
    //double pi = 2*asin(1.0);
    //return pi;
}

ComputePi::~ComputePi()=default; // empty destructor