#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
    Script to read output file of series and plot the results in file or on screen
'''
from __future__ import print_function
import argparse
import matplotlib.pyplot as plt
import numpy as np
import csv 

def main():

    parser = argparse.ArgumentParser(
        description='Read output file of series produced by C++ code and plot results')
    parser.add_argument('-f', '--filename', type=str,
                        help=('Name of file to read, e.g. [ --filename results.txt]'),
                        required=True)
    parser.add_argument('-s', '--save_as', type=str,
                        help=('Name of file to save the figure e.g. [ --save_as my_plot]'),
                        required=False, default="")

    args = parser.parse_args()
    #args = parser.parse_args('--filename my_output.psv --save_as my_plot'.split())
    filename = args.filename
    outfile = args.save_as
    file_extension = filename[-3:]
    
    #initialize variables for plotting 
    x = list()                              #the x-th term of series
    numerical = list()                      #the numerical value of the series up to (x-1)th term
    last_addition = list()                  #the difference applied to value by the x-th term 
    analytic = list()                       #4th column (current difference from analytic prediction)
    
    if (file_extension == "csv" or file_extension == "psv"):
        if (file_extension == "psv"):
            current_delimiter = '|'
        else:
            current_delimiter = ','
            
            
        with open(filename, 'r') as csv_file:
            reader = csv.reader(csv_file, delimiter = current_delimiter)
            lines = list()       
            for row in reader:
                lines.append(row)
                if (len(row)>0):   #check to ignore empty lines
                    appendLists(x, numerical, last_addition, row)
                    if len(row)==4:
                        analytic.append(float(row[3]))
       
        
    elif (file_extension == "txt"):
    
        #open and read .txt file line by line 
        with open(filename,'r') as file_object :
            
            #store lines of file in list         
            lines = file_object.readlines()   
            
            #split line and store produced elements in separate lists 
            for line in lines:
                split_line = line.split()
                
                
                if (len(split_line)>0): #check to ignore empty lines
                    appendLists(x, numerical, last_addition, split_line)
                    if len(lines[0].split()) == 4: 
                        split_line = line.split()          
                        analytic.append(float(split_line[3]))
                        
    else:
        raise RuntimeError("File extension not supported: " + file_extension)
                            
                                  
    #Create plot of series points 
    fig = plt.figure()
    axe = fig.add_subplot(1, 1, 1)
    axe.plot(x, numerical, marker='o', label='Numerical')
    
    #Plot the analytic prediction as a different line 
    if (file_extension == "txt"):
        if len(lines[0].split()) == 4: 
            plotAnalyticPrediction(axe, x, numerical, last_addition, analytic)
    else:
        if len(lines[0]) == 4: 
            plotAnalyticPrediction(axe, x, numerical, last_addition, analytic)
            
    
    axe.set_xlabel(r'$k$')
    axe.set_ylabel(r'Series')
    axe.legend()
    
    if outfile == " ":
        plt.show()
    else: 
        plt.savefig(outfile)
   
def appendLists(x, numerical, last_addition, list_object):

    '''
    Function to append to lists x, numerical and last_addition the elements of list_object 
    
    list_object:      a list of strings (line of the file) 
    x          :      the current list of terms  (1st element of the line)
    numerical  :      the current list of numerical values of the series (2nd element of the line)
    last_addition:    the current list of last term additions (3rd element of the line)
    '''
    x.append(float(list_object[0]))
    numerical.append(float(list_object[1]))
    last_addition.append(float(list_object[2]))
    
def plotAnalyticPrediction(axe, x, numerical, last_addition, analytic):
    
    '''
    Function to calculate the analytical prediction points by adding element-wise the columns of the file 
    and plot them in given axis 
    
    axe          :     current axis where the points will be plotted 
    x            :     the list of terms  (1st column of the file)
    numerical    :     the list of numerical values of the series (2nd column of the file)
    last_addition:     the list of last term additions (3rd column of the file)
    analytic     :     the list of differences from analytic prediction (4rth column of the file) 
    '''
    prediction = np.array(numerical) + np.array(last_addition) + np.array(analytic) #construction of the analytic prediction
    prediction = list(prediction)
    axe.plot(x, prediction, label='Analytical')
    
if __name__ == "__main__":
    main()