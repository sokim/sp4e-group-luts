//
// Created by dimitris on 24.10.20.
//
#include "write_series.h"
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <memory>


WriteSeries::WriteSeries(Series &series, int _frequency, int _maxiter, std::string _separator, std::string _filename) : DumperSeries(series){
    // Parameter initialization
    this->frequency = _frequency;
    this->maxiter = _maxiter;
    this->filename = _filename; // default filename is "output"

    // Set the output file name with extension
    WriteSeries::setSeparator(_separator);
}

void WriteSeries::setSeparator(std::string sep) {
    std::string file_extension;

    if (sep=="comma") {
        this->separator = ",";
        file_extension = ".csv";
    }else if (sep=="pipe") {
        this->separator = "|";
        file_extension = ".psv";
    }else if (sep=="tab") {
        this->separator = "\t";
        file_extension = ".txt";
    }else if (sep=="space") {
        this->separator = " ";
        file_extension = ".txt";
    }else{
        std::cerr << "Check the input argument (--separator "
                  << sep << " )"
                  << "\n OPTIONS: --separator [ space | tab | comma | pipe ]" << std::endl;
        throw std::invalid_argument("Wrong argument values.");
    }

    // Add extension to the filename
    this->filename += file_extension;
    std::cout << "Results are printed to file " << filename << "\n" << std::endl;

}

//dump implementation
/*----------- Using std::ostream -----------------*/
void WriteSeries::dump(std::ostream &os) {

    os << std::setprecision((int) precision);
    os << std::scientific;
    // os << "start of the file \n";
    for (int i = 1; i <= static_cast<int>(maxiter / frequency); ++i){
        double res1 = series.compute(i * frequency - 1);
        double res2 = series.compute(i * frequency);

        if (std::isnan(series.getAnalyticPrediction())){
            os << i * frequency << separator << res1 << separator << res2 - res1 << "\n";
        }
        else{
            os << i * frequency << separator << res1 << separator << res2 - res1 << separator << abs(res2-series.getAnalyticPrediction()) << "\n";
        }
    }
}

/*--------- Simply using std::cout --------------*/
void WriteSeries::dump() {
    //set file name and type
    std::ofstream fout(this->filename);

    fout.precision(precision);
    fout.setf(std::ios::scientific);
    for (int i = 1; i <= static_cast<int>(maxiter / frequency); ++i){
        double res1 = series.compute(i * frequency - 1);
        double res2 = series.compute(i * frequency);

        if (std::isnan(series.getAnalyticPrediction())){
            fout << i * frequency << separator << res1 << separator << res2 - res1 << "\n";
        }
        else{
            fout << i * frequency << separator << res1 << separator << res2 - res1 << separator << abs(res2-series.getAnalyticPrediction()) << "\n";
        }
    }
    fout.close();
}

// Destructor
WriteSeries::~WriteSeries() = default;




