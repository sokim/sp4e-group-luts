//
// Created by Sohyeong Kim on 22.10.20.
//

#include "compute_arithmetic.h"

ComputeArithmetic::ComputeArithmetic(){
    this->computeTerm = [](unsigned int k){return 1. * k;};
}

ComputeArithmetic::~ComputeArithmetic() = default;