//
// Created by dimitris on 23.10.20.
//

#ifndef SP4E_GROUP_LUTS_PRINTSERIES_H
#define SP4E_GROUP_LUTS_PRINTSERIES_H

#include "dumper_series.h"
#include "series.h"

class PrintSeries : public DumperSeries {
public:
    PrintSeries(Series &series, int frequency, int maxiter); //Constructor

    void dump(std::ostream &os = std::cout) override; //dump function using std:ostream (ex4)
    void dump() override; //dump function using std:cout

    virtual ~PrintSeries(); //Destructor

protected:
    int frequency;
    int maxiter;
};

#endif //SP4E_GROUP_LUTS_PRINTSERIES_H
