//
// Created by Sohyeong Kim on 22.10.20.
//
/* -------------------------------------------------------------------------- */
#include <iostream>
#include <string>
#include <memory>
#include <fstream>
/* -------------------------------------------------------------------------- */
#include "argument_parser.h"
#include "compute_arithmetic.h"
#include "compute_pi.h"
#include "print_series.h"
#include "write_series.h"

/* -------------------------------------------------------------------------- */

int main(int argc, char* argv[]){
    /* ------------------------ Parse input arguments ------------------------ */
    ArgumentParser input_arguments(argc, argv);
    auto args = input_arguments.argument_pairs;

    /* ----------------- Save argument values to variables using map ------------------  */
    auto N = std::stoul(args.at("max_iter"));
    auto series_type = args.at("series_type");
    auto frequency = std::stoul(args.at("frequency"));
    auto filename = args.at("filename");
    auto separator = args.at("separator");
    auto output_type = args.at("output_type");
    auto precision = std::stoul(args.at("precision"));

    /* ------------------------------Check N and frequency ----------------------------*/
    if (frequency > N) {
        std::cerr << " Input arguments error: argument --frequency cannot be greater than N (--max_iter) \n" <<
                   frequency << " !<= " << N << "\n"  << std::endl;
        throw std::invalid_argument(" Wrong argument values.");
    };

    /* ------------------------- Series Object Creation ----------------------------- */
    std::unique_ptr<Series> series_object;
    if (series_type == "pi")
        series_object = std::make_unique<ComputePi>();
    else if (series_type == "arithmetic")
        series_object = std::make_unique<ComputeArithmetic>();
    else {
        std::cerr << "Check the input argument (--series_type "
                  << series_type << " )"
                  << "\n OPTIONS: --series_type [ pi | arithmetic ]" << std::endl;
        throw std::invalid_argument("Wrong argument values.");
    }

    /* ---------------- Dumper Object creation and Output Setup ---------------------- */
    std::unique_ptr<DumperSeries> dumper_object;
    std::ostream* output;
    if (output_type == "screen") {
        dumper_object = std::make_unique<PrintSeries>(*series_object, frequency, N);
        output = &std::cout;
    }
    else if(output_type == "file"){
        dumper_object = std::make_unique<WriteSeries>(*series_object, frequency, N, separator, filename);
        output = new std::ofstream(dumper_object->filename);
    }
    else{
        std::cerr << "Check the input argument (--output_type "
                  << series_type << " )"
                  << "\n OPTIONS: --output_type [ screen | file ]" << std::endl;
        throw std::invalid_argument("Wrong argument values.");
    }

    /* --------------------------------- Do the Job --------------------------------- */
    dumper_object->setPrecision(precision);
    //dumper_object->dump();   //Command to dump by using simple std::cout
    *output << *dumper_object << std::endl;   //Command to dump by using ostream

    /*------------------------------ Release the memory ------------------------------*/
    if (output_type == "file"){
        delete output;
    }

    return 0;
}