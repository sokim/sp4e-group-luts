//
// Created by Sohyeong Kim on 23.10.20.
//

#include "argument_parser.h"
#include <iostream>
#include <sstream>

void PrintArgumentError(int err_code){
    switch(err_code){
        case 1:
            std::cerr << "Not enough input arguments: --series_type argument is required. " <<
                      "\n OPTIONS: --series_type [ pi | arithmetic ]" << std::endl;
            ArgumentParser::showUsageExample();
            throw std::invalid_argument("Not enough input arguments");
        case 2:
            std::cerr << "Argument name-value mismatch. For every argument name input a valid option." << std::endl;
            ArgumentParser::showUsageExample();
            throw std::invalid_argument("Argument name-value mismatch");
        case 3:
            std::cerr << "Argument name error. The argument name that you typed is not a valid argument name." << std::endl;
            ArgumentParser::showUsageExample();
            throw std::invalid_argument("Argument name unknown");
        default:
            std::cerr << "Not specified argument error." << std::endl;
            ArgumentParser::showUsageExample();
            throw std::invalid_argument("Unspecified argument error.");
    }
}

ArgumentParser::ArgumentParser(int argc, char* argv[]) {
    this->arguments_count = argc;
    std::string key, value;

    // When no arguments entered, print error message (--series_type needed)
    if(arguments_count == 1) PrintArgumentError(1);

    // When even number of arguments entered, print error message
    if(arguments_count % 2 == 0){
        this->arguments_stream.clear();
        this->arguments_stream.str(argv[1]);
        arguments_stream >> key;
        // if the input is help, print out the argument example
        if ( key == "--help"){
            ArgumentParser::showUsageExample();
            std::exit(0);
        }
        else PrintArgumentError(2); //otherwise print error
    }

        // When more than two arguments are entered
    else {
        int num_arg_pairs = (int) (arguments_count -1) /2;

        for (int i = 1; i <= num_arg_pairs; i++) {
            this->arguments_stream.clear();
            this->arguments_stream.str(argv[2*i-1]);  //Read key from argv
            arguments_stream >> key;

            // When the key is correct, replace the default argument value with the one parsed in argv
            if(argument_pairs.count(key.substr(2))==1){
                this->arguments_stream.clear();
                this->arguments_stream.str(argv[2*i]); //Read value from argv
                arguments_stream >> value;

                argument_pairs[key.substr(2)] = value;
            }
            // When the key is not correct, print error message
            else PrintArgumentError(3);
        }
    }
}

void ArgumentParser::showUsageExample() {
    std::cerr << "ARGUMENT OPTIONS: \n"
                 "\t--series_type [ arithmetic | pi ] \n"
                 "\t--max_iter [<unsigned int>] \n"
                 "\t--frequency [<unsigned int>] \n"
                 "\t--output_type [ screen | file ] \n"
                 "\t--filename [<filename>] \n"
                 "\t--separator [ space | tab | comma | pipe ] \n"
                 "\t--precision [<unsigned int>] \n" << std::endl;
}

ArgumentParser::~ArgumentParser() = default;

