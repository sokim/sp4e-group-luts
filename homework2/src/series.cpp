//
// Created by Sohyeong Kim on 22.10.20.
//

#include "series.h"

Series::Series() {}

// Series member function
double Series::compute(unsigned int N) {
    if (current_index <= N){
        N -= current_index;
    }
    else{
        current_value = 0.;
        current_index = 0;
    }

    for(unsigned int k=0; k < N; ++k){
        ++current_index;
        current_value += computeTerm(current_index);
    }

    return current_value;
}

