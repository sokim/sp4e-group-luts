//
// Created by dimitris on 23.10.20.
//
#include "dumper_series.h"

DumperSeries::DumperSeries(Series &series) : series{series}{}

void DumperSeries::setPrecision(unsigned int _precision) {
    this->precision = _precision;
}

DumperSeries::~DumperSeries() = default;