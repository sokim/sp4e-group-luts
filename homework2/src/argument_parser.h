//
// Created by Sohyeong Kim on 23.10.20.
//

#ifndef HW2_CPP_CLASSES_ARGUMENT_PARSER_H
#define HW2_CPP_CLASSES_ARGUMENT_PARSER_H

#include <map>
#include <string>
#include <vector>
#include <sstream>

class ArgumentParser {

public:
    //Use std::map to store argument keys and values
    using ArgStruct = std::map<std::string, std::string>;
    // Default values of each option;
    ArgStruct argument_pairs ={
            {"series_type", "arithmetic"}, //"arithmetic" or "pi"
            {"max_iter", "10"},
            {"frequency", "1"},
            {"filename", "output"},
            {"separator", "space"}, // "space", "tab", "pipe", "comma"
            {"output_type", "screen"}, //"screen" or "file"
            {"precision", "10"}
    };
    ArgumentParser(int argc, char* argv[]); // Constructor
    ~ArgumentParser(); // Destructor
    static void showUsageExample();
private:
    int arguments_count;
    std::stringstream arguments_stream;
};


#endif //HW2_CPP_CLASSES_ARGUMENT_PARSER_H
