//
// Created by Sohyeong Kim on 22.10.20.
//

#ifndef SP4E_GROUP_LUTS_SERIES_H
#define SP4E_GROUP_LUTS_SERIES_H

#include <functional>
#include <cmath>
#include <math.h>


class Series {

public:
    Series();
    virtual double compute(unsigned int N) ;
    std::function<double(unsigned int)> computeTerm;
    virtual double getAnalyticPrediction(){ return std::nan(""); };

protected:
    unsigned int current_index = 0;
    double current_value = 0.;

};


#endif //SP4E_GROUP_LUTS_SERIES_H
