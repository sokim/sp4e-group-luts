//
// Created by Sohyeong Kim on 22.10.20.
//

#ifndef SP4E_GROUP_LUTS_COMPUTEARITHMETIC_H
#define SP4E_GROUP_LUTS_COMPUTEARITHMETIC_H

#include "series.h"


class ComputeArithmetic : public Series{
public:
    // Constructor
    ComputeArithmetic();

    // Destructor
    ~ComputeArithmetic();

};


#endif //SP4E_GROUP_LUTS_COMPUTEARITHMETIC_H
