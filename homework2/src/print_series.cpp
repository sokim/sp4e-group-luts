//
// Created by dimitris on 23.10.20.
//
#include <iostream>
#include "print_series.h"
#include <cmath>
#include <iomanip>

//Constructor implementation
PrintSeries::PrintSeries(Series &series, int frequency, int maxiter) : DumperSeries(series){
    this->frequency = frequency;
    this->maxiter = maxiter;
}

//dump implementation
/*----------- Using std::ostream -----------------*/
void PrintSeries::dump(std::ostream &os) {
    os << std::setprecision(precision);
    os << std::scientific;



    for (int i = 1; i <= static_cast<int>(maxiter / frequency); ++i) {
        double res1 = series.compute(i * frequency - 1);
        double res2 = series.compute(i * frequency);

        if (std::isnan(series.getAnalyticPrediction())){
            if (i ==1) os << "[iteration | series computation result | error] \n";
            os << i * frequency << " " << res1 << " " << res2 - res1 << "\n";
        }
        else{
            if (i==1) os << "[iteration | series computation result | error | Analytic prediction error] \n";
            os << i * frequency << " " << res1 << " " << res2 - res1 << " " << abs(res2-series.getAnalyticPrediction()) << "\n";
        }
    }

}

/*--------- Simply using std::cout --------------*/
void PrintSeries::dump(){
    std::cout.precision(precision);
    std::cout.setf(std::ios::scientific);


    for (int i = 1; i <= static_cast<int>(maxiter / frequency); ++i){
        double res1 = series.compute(i * frequency - 1);
        double res2 = series.compute(i * frequency);

        if (std::isnan(series.getAnalyticPrediction())){
            std::cout << i * frequency << " " << res1 << " " << res2 - res1 << "\n";
        }
        else{
            std::cout << i * frequency << " " << res1 << " " << res2 - res1 << " " << abs(res2-series.getAnalyticPrediction()) << "\n";
        }
    }
}

PrintSeries::~PrintSeries()= default;


